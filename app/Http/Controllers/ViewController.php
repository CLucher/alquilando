<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function pages($name)
    {
        return view('pages.'.$name);
    }

    public function components($name)
    {
        return view('components.'.$name);
    }
}