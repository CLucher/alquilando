/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(10);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/*require('./components/controller')
require('./components/component')*/
__webpack_require__(2);
__webpack_require__(3);
__webpack_require__(4);
__webpack_require__(5);
__webpack_require__(6);
__webpack_require__(7);
__webpack_require__(8);
__webpack_require__(9);

/***/ }),
/* 2 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app', ['ngMessages', 'ngRoute', 'ngResource', 'ngAnimate', 'ngTable']);
})();

/***/ }),
/* 3 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').run(runBlock);

    runBlock.$inject = ['$rootScope', '$window'];

    function runBlock($rootScope, window) {

        configRouteChange($rootScope);

        function configRouteChange($rootScope) {
            /* $rootScope.$on('$routeChangeStart', function(event, next, current) {
                 $window.location.href = '/users';
             })*/
        }
    }
})();

/***/ }),
/* 4 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$routeProvider', '$qProvider'];

    function config($routeProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false);

        $routeProvider.when('/', {
            redirectTo: '/users'
        }).when('/users', {
            templateUrl: 'view/pages/users',
            controller: 'UsersController',
            controllerAs: 'userCtrl',
            resolve: {
                users: ['usersService', function (usersService) {
                    return usersService.api.query().$promise;
                }]
            }
        }).otherwise({
            redirectTo: '/'
        });
    }
})();

/***/ }),
/* 5 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').service('usersService', usersService);

    usersService.$inject = ['$resource'];

    function usersService($resource) {
        var self = this;

        self.api = $resource('/users/:id', { id: '@id' }, { 'update': { method: 'PUT' } });
    }
})();

/***/ }),
/* 6 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('UsersController', UsersController);

    UsersController.$inject = ['$rootScope', '$scope', '$window', 'NgTableParams', 'users', 'usersService'];

    function UsersController($rootScope, $scope, $window, NgTableParams, users, usersService) {
        var vm = this;

        vm.tableParams = new NgTableParams({}, { dataset: users, counts: [] });

        vm.add = function () {
            $scope.$broadcast('user-new');
        };

        vm.edit = function (user, index) {
            $scope.$broadcast('user-edit', { data: angular.copy(user), index: index });
        };

        vm.confirmDelete = function (user, index) {
            usersService.api.delete({ id: user.id }).$promise.then(function (res) {
                vm.tableParams.data.splice(index, 1);
            });
        };

        $scope.$on('user-edit-save', function (event, obj) {
            vm.tableParams.data[obj.index] = obj.data;
        });

        $scope.$on('user-new-save', function (event, obj) {
            vm.tableParams.data.push(obj.data);
        });
    }
})();

/***/ }),
/* 7 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').component('userEdit', {
        templateUrl: '/view/components/user-edit',
        controller: 'UserEditController',
        controllerAs: 'userEditCtrl'
    });
})();

/***/ }),
/* 8 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('UserEditController', UserEditController);

    UserEditController.$inject = ['$rootScope', '$scope', 'usersService'];

    function UserEditController($rootScope, $scope, usersService) {
        var vm = this;

        $scope.$on('user-edit', function (event, obj) {
            vm.data = obj.data;
            vm.index = obj.index;
            vm.active = true;
            vm.edit = true;
        });

        $scope.$on('user-new', function (event, obj) {
            vm.active = true;
            vm.edit = false;
        });

        vm.close = function () {
            reset();
        };

        vm.submit = function () {
            vm.submitted = true;

            if (vm.userForm.$valid) send();
        };

        function send() {
            vm.sending = true;

            if (vm.edit) edit();else save();
        }

        function edit() {
            usersService.api.update({ id: vm.data.id }, vm.data).$promise.then(function (res) {
                $scope.$emit('user-edit-save', { data: res, index: vm.index });
                vm.close();
            }).catch(showErrors).finally(function () {
                vm.sending = false;
            });
        }

        function save() {
            usersService.api.save(vm.data).$promise.then(function (res) {
                $scope.$emit('user-new-save', { data: res });
                vm.close();
            }).catch(showErrors).finally(function () {
                vm.sending = false;
            });
        }

        function showErrors(err) {
            for (var i in err.data) {
                return vm.responseError = err.data[i][0];
            }
        }

        function reset() {
            vm.active = false;
            vm.submitted = false;
            vm.userForm.$setPristine();
            vm.data = null;
            vm.index = null;
            vm.responseError = null;
        }
    }
})();

/***/ }),
/* 9 */
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').value('messages', {

        USER_CONFIRM_DELETE_MESSAGE: 'Are You sure?',
        USER_CONFIRM_DELETE_TITLE: 'Users',
        USER_NOTIFICATION_SAVE: 'Was successfully save',
        USER_NOTIFICATION_UPDATED: 'Was successfully updated'

    });
})();

/***/ }),
/* 10 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);