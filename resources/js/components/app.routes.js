(function() {
    'use strict'

    angular
        .module('app')
        .config(config)

    config.$inject = ['$routeProvider', '$qProvider']

    function config($routeProvider, $qProvider) {

        $qProvider.errorOnUnhandledRejections(false)

        $routeProvider
            .when('/', {
                redirectTo:'/users'
            })
             .when('/users', {
                templateUrl: 'view/pages/users',
                controller: 'UsersController',
                controllerAs: 'userCtrl',
                resolve: {
                    users: ['usersService', (usersService) => {
                        return usersService.api.query().$promise
                    }],
                }
            })
            .otherwise({
                redirectTo:'/'
            })
    }
})();
