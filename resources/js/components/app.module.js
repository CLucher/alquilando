(function() {
    'use strict'

    angular
        .module('app', [
        	'ngMessages',
            'ngRoute',
            'ngResource',
            'ngAnimate',
            'ngTable',
        ])
})();