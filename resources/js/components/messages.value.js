(function() {
    'use strict';

    angular
        .module('app')
        .value('messages', {

            USER_CONFIRM_DELETE_MESSAGE: 'Are You sure?',
            USER_CONFIRM_DELETE_TITLE: 'Users',
            USER_NOTIFICATION_SAVE: 'Was successfully save',
            USER_NOTIFICATION_UPDATED: 'Was successfully updated',

        });
})();