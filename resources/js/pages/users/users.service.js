(function() {
    'use strict'

    angular
        .module('app')
        .service('usersService', usersService)

    usersService.$inject = ['$resource']

    function usersService($resource) {
        var self = this

       self.api = $resource('/users/:id', {id: '@id'}, {'update': { method:'PUT' }})

    }
})();