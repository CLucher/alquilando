(function() {
    'use strict';

    angular
        .module('app')
        .component('userEdit', {
            templateUrl: '/view/components/user-edit',
            controller: 'UserEditController',
            controllerAs: 'userEditCtrl',
        });
})();