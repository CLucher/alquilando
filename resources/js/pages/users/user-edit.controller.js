(function() {
    'use strict'

    angular
        .module('app')
        .controller('UserEditController', UserEditController)

    UserEditController.$inject = ['$rootScope','$scope', 'usersService']

    function UserEditController($rootScope, $scope, usersService) {
        var vm = this

        $scope.$on('user-edit', (event, obj) => {
            vm.data = obj.data
            vm.index = obj.index
            vm.active = true
            vm.edit = true
        })
       
        $scope.$on('user-new', (event, obj) => {
            vm.active = true
            vm.edit = false
        })

        vm.close = function() {
            reset()
        }

        vm.submit = function () {
            vm.submitted = true
            
            if(vm.userForm.$valid)
                send()
        }

        function send() {
            vm.sending = true
            
            if(vm.edit)
                edit()
            else
                save()
        }

        function edit() {
            usersService.api.update({ id: vm.data.id }, vm.data)
            .$promise.then((res) => {
                $scope.$emit('user-edit-save', { data: res, index: vm.index })
                vm.close()
            }).catch(showErrors)
            .finally(() => {
                vm.sending = false
            })
        }

 
        function save() {
            usersService.api.save(vm.data)
            .$promise.then((res) => {
                $scope.$emit('user-new-save', { data: res })
                vm.close()
            }).catch(
                showErrors
            ).finally(() => {
                vm.sending = false
            })
        }

        function showErrors(err) {
            for(let i in (err.data))
                return vm.responseError = err.data[i][0] 
        }

        function reset() {
            vm.active = false
            vm.submitted = false
            vm.userForm.$setPristine()
            vm.data = null
            vm.index = null
            vm.responseError = null
        }

    }

})();