(function() {
    'use strict'

    angular
        .module('app')
        .controller('UsersController', UsersController)

    UsersController.$inject = ['$rootScope', '$scope', '$window', 'NgTableParams', 'users', 'usersService' ]

    function UsersController($rootScope, $scope, $window ,NgTableParams, users , usersService) {
        var vm = this

        vm.tableParams = new NgTableParams({}, { dataset: users, counts: [] })

        vm.add = function() {
            $scope.$broadcast('user-new')
        }

        vm.edit = function(user, index) {
            $scope.$broadcast('user-edit', { data: angular.copy(user), index: index })
        }        

        vm.confirmDelete = function (user, index) {
            usersService.api.delete({ id: user.id })
            .$promise.then((res) => {
                vm.tableParams.data.splice(index, 1)
            })
        }

        $scope.$on('user-edit-save', (event, obj) => {
            vm.tableParams.data[obj.index] = obj.data
        })        

        $scope.$on('user-new-save', (event, obj) => {
            vm.tableParams.data.push(obj.data)
        })
    }
})();