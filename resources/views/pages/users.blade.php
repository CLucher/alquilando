<div class="table-container">
    <table class="table table-data table-show" ng-table="userCtrl.tableParams">
        <tbody>
            <tr  ng-repeat="u in $data | filter: userCtrl.search track by $index" ng-click="userCtrl.show(c, $index)">
                <td title="'name'" ng-bind="u.name"></td>
                <td title="'last name'" ng-bind="u.last_name"></td>
                <td title="'email'" ng-bind="u.email"></td>
                <td title="'user'" ng-bind="u.user"></td>
                <td class="td-action" header-class="'th-action'" title="Edit">
                    <button class="button button-icon" ng-click="userCtrl.edit(u, $index); $event.stopPropagation();"><i class="material-icons">edit</i></button>
                </td>
                <td class="td-action" header-class="'th-action'" title="Delete">
                    <button class="button button-icon" ng-click="userCtrl.confirmDelete(u, $index); $event.stopPropagation();"><i class="material-icons">delete</i></button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

    <div class="button-group" >
        <button class="button button-action center" ng-click="userCtrl.add()" title="Add new buyer"><i class="material-icons">add</i></button>
    </div>

<user-edit></user-edit>
