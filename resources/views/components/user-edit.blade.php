<modal class="modal modal-move modal-rigth" ng-class="{ active: userEditCtrl.active }" active="customerEditCtrl.active">
    <div class="modal-content modal-box modal-edit">
        <div class="modal-header">
            <div>User</div>
            <button class="button button-icon" ng-click="userEditCtrl.close()"><i class="material-icons">close</i></button>
        </div>
        <form class="form modal-body modal-scroll" name="userEditCtrl.userForm" ng-submit="userEditCtrl.submit()" novalidate autocomplete="off">
            <label class="input-group">
                <input class="input" ng-model="userEditCtrl.data.name" required>
                <div class="input-label">
                    <i class="material-icons">person</i>
                    <span>Name</span>
                </div>
            </label>
            <label class="input-group">
                <input class="input" type="text" ng-model="userEditCtrl.data.last_name">
                <div class="input-label">
                    <i class="material-icons">business</i>
                    <span>Last name</span>
                </div>
            </label>
            <label class="input-group">
                <input class="input" type="text" ng-model="userEditCtrl.data.email">
                <div class="input-label">
                    <i class="material-icons">account_balance</i>
                    <span>Email</span>
                </div>
            </label>
            <label class="input-group">
                <input class="input" type="text" ng-model="userEditCtrl.data.user">
                <div class="input-label">
                    <i class="material-icons">location_on</i>
                    <span>User</span>
                </div>
            </label>
            <div class="input-errors" ng-messages="userEditCtrl.userForm.$error" ng-if="userEditCtrl.submitted" role="alert">
                <div class="input-error-msg" ng-message="required">*Required</div>
            </div>
            <div class="input-errors" ng-if="userEditCtrl.submitted && userEditCtrl.responseError" role="alert">
                <div ng-bind="userEditCtrl.responseError"></div>
            </div>
            <div class="button-group">
                <button class="button button-primary" ng-disabled="userEditCtrl.sending">Save</button>
            </div>
        </form>
    </div>
</modal>